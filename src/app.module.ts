import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { CommandModule } from 'nestjs-command';
import { GraphQLModule } from '@nestjs/graphql';
import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { OrderModule } from './e-commerce/order/order.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { UserModule } from './user/user.module';
import { join } from 'path';
import { ConversationModule } from './chat/conversation/conversation.module';
import { SearchModule } from './search/search.module';
import { CityModule } from './local/city/city.module';
import { DistrictModule } from './local/district/district.module';
import { WardModule } from './local/ward/ward.module';
import { StreetModule } from './local/street/street.module';
import { MessageModule } from './chat/message/message.module';
import { ProductModule } from './e-commerce/product/product.module';
import { VoucherModule } from './e-commerce/voucher/voucher.module';
import { CategoryModule } from './e-commerce/category/category.module';
import { graphqlUploadExpress } from 'graphql-upload';
import { FileModule } from './file/file.module';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    GraphQLModule.forRoot({
      autoSchemaFile: 'schema.gql',
      debug: false,
      playground: true,
      installSubscriptionHandlers: true,
      context: ({ req, connection }) =>
        connection ? { req: connection.context } : { req },
      uploads: false,
    }),
    CommandModule,
    MongooseModule.forRoot('mongodb://localhost:27017/server', {
      useFindAndModify: false,
    }),
    UserModule,
    AuthModule,
    ProductModule,
    CategoryModule,
    OrderModule,
    CityModule,
    DistrictModule,
    WardModule,
    StreetModule,
    VoucherModule,
    ConversationModule,
    MessageModule,
    SearchModule,
    FileModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(graphqlUploadExpress()).forRoutes('graphql');
  }
}
