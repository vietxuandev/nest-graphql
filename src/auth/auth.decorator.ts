import { SetMetadata, UseGuards, applyDecorators } from '@nestjs/common';

import { AuthGuard } from './auth.guard';
import { RolesGuard } from 'src/common/guards/roles.guard';

export function Auth(...roles: string[]) {
  return applyDecorators(
    SetMetadata('roles', roles),
    UseGuards(AuthGuard, RolesGuard),
  );
}
