import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import {
  HttpException,
  HttpStatus,
  UnauthorizedException,
  UseGuards,
} from '@nestjs/common';
import { UserService } from 'src/user/user.service';
import { AuthService } from './auth.service';
import { User } from 'src/user/user.schema';
import { AuthGuard } from './auth.guard';
import { CurrentUser } from 'src/user/user.decorator';
import { UserOutput } from 'src/user/dto/user.output';
import { SignInInput, SignUpInput } from 'src/user/dto/user.input';
import { OrderService } from 'src/e-commerce/order/order.service';

@Resolver()
export class AuthResolver {
  constructor(
    private readonly userService: UserService,
    private readonly authService: AuthService,
    private readonly orderService: OrderService,
  ) {}

  @Query(() => UserOutput)
  @UseGuards(AuthGuard)
  currentUser(@CurrentUser() user: User) {
    return user;
  }

  @Mutation(() => String)
  async signUp(@Args('input') input: SignUpInput) {
    const user = await this.userService.findByEmail(input.email);
    if (!user) {
      const newUser = await this.userService.create(input);
      await this.orderService.findAndUpdate(
        { phoneNumber: newUser.phoneNumber },
        { $set: { user: newUser._id } },
      );
      return this.authService.createToken(newUser);
    }
    throw new HttpException(
      'This email is already registered',
      HttpStatus.CONFLICT,
    );
  }

  @Mutation(() => String)
  async signIn(@Args('input') input: SignInInput) {
    const user = await this.authService.validateUser(
      input.email,
      input.password,
    );
    if (!user) {
      throw new UnauthorizedException();
    }
    return await this.authService.createToken(user);
  }
}
