import { Conversation, ConversationSchema } from './conversation.schema';

import AutoPopulate from 'mongoose-autopopulate';
import { ConversationResolver } from './conversation.resolver';
import { ConversationService } from './conversation.service';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SearchModule } from 'src/search/search.module';
import { ConversationsSearchService } from './conversationsSearch.service';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: Conversation.name,
        useFactory: () => {
          const schema = ConversationSchema;
          schema.plugin(AutoPopulate);
          return schema;
        },
      },
    ]),
    SearchModule,
  ],
  providers: [
    ConversationService,
    ConversationsSearchService,
    ConversationResolver,
  ],
  exports: [ConversationService, ConversationsSearchService],
})
export class ConversationModule {}
