import { HttpException, HttpStatus, UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { PubSub } from 'apollo-server-express';
import { UserOutput } from 'src/user/dto/user.output';
import { CurrentUser } from 'src/user/user.decorator';
import { UserGuard } from 'src/user/user.guard';
import { User } from 'src/user/user.schema';
import { ConversationService } from './conversation.service';
import { ConversationInput } from './dto/conversation.input';
import { ConversationOutput, TypingOutput } from './dto/conversation.output';

@Resolver()
export class ConversationResolver {
  private pubSub: PubSub;
  constructor(private readonly conversationService: ConversationService) {
    this.pubSub = new PubSub();
  }
  @Query(() => [ConversationOutput])
  async conversations(
    @Args('searchText', { type: () => String, nullable: true })
    searchText?: string,
  ) {
    if (searchText) {
      return await this.conversationService.searchForConversation(searchText);
    }
    return await this.conversationService.findAll();
  }
  @Mutation(() => ConversationOutput)
  @UseGuards(UserGuard)
  async createConversation(
    @Args('input') input: ConversationInput,
    @CurrentUser() currentUser: User,
  ) {
    input.participants.push(currentUser._id);
    return await this.conversationService.create(input);
  }
  @Query(() => ConversationOutput)
  async conversation(
    @Args('conversationId', { type: () => String })
    conversationId: string,
  ) {
    return await this.conversationService.findById(conversationId);
  }

  @Mutation(() => TypingOutput)
  @UseGuards(UserGuard)
  async typing(
    @Args('conversationId', { type: () => String })
    conversationId: string,
    @CurrentUser() currentUser: User,
    @Args('typing', { type: () => Boolean })
    typing: boolean,
  ) {
    const conversation = await this.conversationService.findById(
      conversationId,
    );
    if (
      conversation.participants.find(
        item => item._id.toString() === currentUser._id,
      )
    ) {
      const newTyping = { user: currentUser, conversation, typing: typing };
      this.pubSub.publish('newTyping', { newTyping });
      return newTyping;
    } else {
      throw new HttpException(
        'Conversation Id is not available',
        HttpStatus.FORBIDDEN,
      );
    }
  }

  @UseGuards(UserGuard)
  @Subscription(() => TypingOutput, {
    filter: (
      { newTyping }: { newTyping: TypingOutput },
      { conversation }: { conversation: string },
      { user }: { user: UserOutput },
    ) => {
      return (
        conversation === newTyping.conversation._id.toString() &&
        !!newTyping.conversation.participants.find(
          item => item._id.toString() === user._id,
        ) &&
        user._id !== newTyping.user._id
      );
    },
  })
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  newTyping(@Args('conversation') conversation: string) {
    return this.pubSub.asyncIterator('newTyping');
  }
}
