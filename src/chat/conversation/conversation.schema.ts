import { Document, Types } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

import { User } from 'src/user/user.schema';

@Schema({ timestamps: true })
export class Conversation extends Document {
  @Prop({ type: String, required: true })
  name: string;

  @Prop([
    {
      type: [Types.ObjectId],
      ref: User.name,
      required: true,
      autopopulate: true,
    },
  ])
  participants: User[];
}

export const ConversationSchema = SchemaFactory.createForClass(Conversation);
