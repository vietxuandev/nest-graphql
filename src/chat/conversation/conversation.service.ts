import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Conversation } from './conversation.schema';
import { ConversationsSearchService } from './conversationsSearch.service';
import { ConversationInput } from './dto/conversation.input';

@Injectable()
export class ConversationService {
  constructor(
    @InjectModel(Conversation.name)
    private readonly conversationModel: Model<Conversation>,
    private readonly conversationsSearchService: ConversationsSearchService,
  ) {}

  async findById(id: string): Promise<Conversation | undefined> {
    return this.conversationModel.findById(id);
  }

  async findAll(): Promise<Conversation[]> {
    return this.conversationModel.find().exec();
  }

  async searchForConversation(text: string) {
    const results = await this.conversationsSearchService.search(text);
    const ids = results.map(result => result.id);
    if (!ids.length) {
      return [];
    }
    return this.conversationModel.find({ _id: { $in: ids } }).exec();
  }

  async create(input: ConversationInput): Promise<Conversation | undefined> {
    const createdConversation = new this.conversationModel(input);
    return createdConversation.save();
  }
}
