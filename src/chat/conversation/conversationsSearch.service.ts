import { Injectable } from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { Conversation } from './conversation.schema';
import ConversationSearchBody from './types/conversationSearchBody.interface';
import ConversationSearchResult from './types/conversationSearchResult.interface';

@Injectable()
export class ConversationsSearchService {
  index = 'conversations';
  constructor(private readonly elasticsearchService: ElasticsearchService) {}

  async indexConversation(conversation: Conversation) {
    return this.elasticsearchService.index<
      ConversationSearchResult,
      ConversationSearchBody
    >({
      index: this.index,
      body: {
        id: conversation.id,
        name: conversation.name,
      },
    });
  }

  async search(text: string) {
    const { body } = await this.elasticsearchService.search<
      ConversationSearchResult
    >({
      index: this.index,
      body: {
        query: {
          multi_match: {
            query: text,
            fields: ['content'],
          },
        },
      },
    });
    const hits = body.hits.hits;
    return hits.map(item => item._source);
  }
}
