import { Field, ID, InputType } from '@nestjs/graphql';

@InputType()
export class ConversationInput {
  @Field(() => String, { nullable: false })
  name: string;

  @Field(() => [ID], { nullable: false })
  participants: string[];
}
