import { Field, ID, ObjectType } from '@nestjs/graphql';

import { UserOutput } from 'src/user/dto/user.output';

@ObjectType()
export class ConversationOutput {
  @Field(() => ID, { nullable: true })
  _id?: string;

  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => [UserOutput], { nullable: true })
  participants?: UserOutput[];
}

@ObjectType()
export class TypingOutput {
  @Field(() => UserOutput, { nullable: true })
  user?: UserOutput;

  @Field(() => ConversationOutput, { nullable: true })
  conversation?: ConversationOutput;

  @Field(() => Boolean, { nullable: true })
  typing?: boolean;
}
