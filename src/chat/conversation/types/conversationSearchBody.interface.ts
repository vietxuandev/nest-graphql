export default interface ConversationSearchBody {
  id: number;
  name: string;
}
