import ConversationSearchBody from './conversationSearchBody.interface';

export default interface ConversationSearchResult {
  hits: {
    total: number;
    hits: Array<{
      _source: ConversationSearchBody;
    }>;
  };
}
