import { Field, ID, InputType, OmitType, PartialType } from '@nestjs/graphql';
import { GraphQLUpload } from 'graphql-upload';

import { MessageStatus } from 'src/common/enums/message-status.enum';
import { File } from 'src/file/file.schema';

@InputType()
export class MessageBase {
  @Field(() => String, { nullable: false })
  _id: string;

  @Field(() => ID, { nullable: false })
  conversation: string;

  @Field(() => ID, { nullable: false })
  sender: string;

  @Field(() => String, { nullable: true })
  content?: string;

  @Field(() => [GraphQLUpload], { nullable: true })
  files?: File[];

  @Field(() => MessageStatus, { nullable: false })
  status: MessageStatus;

  @Field(() => ID, { nullable: true })
  message?: string;
}

@InputType()
export class MessageSenderInput extends PartialType(MessageBase) {}

@InputType()
export class MessageInput extends PartialType(
  OmitType(MessageBase, ['sender']),
) {}
