import { Field, ID, ObjectType } from '@nestjs/graphql';

import { ConversationOutput } from 'src/chat/conversation/dto/conversation.output';
import { MessageStatus } from 'src/common/enums/message-status.enum';
import { FileOutput } from 'src/file/dto/file.output';
import { UserOutput } from 'src/user/dto/user.output';

@ObjectType()
export class MessageOutput {
  @Field(() => ID, { nullable: true })
  _id?: string;

  @Field(() => ConversationOutput, { nullable: true })
  conversation?: ConversationOutput;

  @Field(() => UserOutput, { nullable: true })
  sender?: UserOutput;

  @Field(() => String, { nullable: true })
  content?: string;

  @Field(() => [FileOutput], { nullable: true })
  files?: FileOutput[];

  @Field(() => MessageStatus, { nullable: true })
  status?: MessageStatus;

  @Field(() => Date, { nullable: true })
  createdAt?: Date;

  @Field(() => MessageOutput, { nullable: true })
  message?: string;
}

@ObjectType()
export class LastSeenOutput {
  @Field(() => UserOutput, { nullable: true })
  currentUser?: UserOutput;

  @Field(() => ConversationOutput, { nullable: true })
  conversation?: ConversationOutput;
}
