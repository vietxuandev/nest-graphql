import { Message, MessageSchema } from './message.schema';

import AutoPopulate from 'mongoose-autopopulate';
import { MessageResolver } from './message.resolver';
import { MessageService } from './message.service';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SearchModule } from 'src/search/search.module';
import { MessagesSearchService } from './messagesSearch.service';
import { ConversationModule } from '../conversation/conversation.module';
import { FileModule } from 'src/file/file.module';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: Message.name,
        useFactory: () => {
          const schema = MessageSchema;
          schema.plugin(AutoPopulate);
          return schema;
        },
      },
    ]),
    SearchModule,
    ConversationModule,
    FileModule,
  ],
  providers: [MessageService, MessagesSearchService, MessageResolver],
})
export class MessageModule {}
