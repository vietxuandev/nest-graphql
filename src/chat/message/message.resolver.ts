import {
  HttpException,
  HttpStatus,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { PubSub } from 'apollo-server-express';
import { UploadedFiles } from 'src/file/file.decorator';
import { GqlFileInterceptor } from 'src/file/file.interceptor';
import { File } from 'src/file/file.schema';
import { UserOutput } from 'src/user/dto/user.output';
import { CurrentUser } from 'src/user/user.decorator';
import { UserGuard } from 'src/user/user.guard';
import { User } from 'src/user/user.schema';
import { ConversationService } from '../conversation/conversation.service';
import { MessageInput } from './dto/message.input';
import { LastSeenOutput, MessageOutput } from './dto/message.output';
import { MessageService } from './message.service';

@Resolver()
export class MessageResolver {
  private pubSub: PubSub;
  constructor(
    private readonly messageService: MessageService,
    private readonly conversationService: ConversationService,
  ) {
    this.pubSub = new PubSub();
  }

  @Mutation(() => MessageOutput)
  @UseInterceptors(GqlFileInterceptor(['files']))
  @UseGuards(UserGuard)
  async createMessage(
    @Args('input') input: MessageInput,
    @CurrentUser() currentUser: User,
    @UploadedFiles() uploadedFiles: { files: File[] },
  ) {
    const dataInput = {
      ...input,
      sender: currentUser._id,
    };
    dataInput.files = uploadedFiles['files'];
    const newMessage = await this.messageService.create(dataInput);
    this.pubSub.publish('newMessage', { newMessage });
    return newMessage;
  }

  @Mutation(() => LastSeenOutput)
  @UseGuards(UserGuard)
  async lastSeen(
    @Args('conversationId', { type: () => String })
    conversationId: string,
    @CurrentUser() currentUser: User,
  ) {
    const conversation = await this.conversationService.findById(
      conversationId,
    );
    if (
      conversation.participants.find(
        item => item._id.toString() === currentUser._id,
      )
    ) {
      await this.messageService.updateManyStatus(
        currentUser._id.toString(),
        conversation._id.toString(),
      );
      this.pubSub.publish('newLastSeen', {
        newLastSeen: { currentUser, conversation },
      });
      return { currentUser, conversation };
    } else {
      throw new HttpException(
        'Conversation Id is not available',
        HttpStatus.FORBIDDEN,
      );
    }
  }

  @Query(() => [MessageOutput])
  @UseGuards(UserGuard)
  async messages(
    @Args('conversationId', { type: () => String })
    conversationId: string,
    @CurrentUser() currentUser: User,
    @Args('offset', { type: () => Number, nullable: true })
    offset?: number,
    @Args('limit', { type: () => Number, nullable: true })
    limit?: number,
  ) {
    const conversation = await this.conversationService.findById(
      conversationId,
    );
    if (
      conversation.participants.find(
        item => item._id.toString() === currentUser._id,
      )
    ) {
      return await this.messageService.findByConversation(
        conversationId,
        offset,
        limit,
      );
    } else {
      throw new HttpException(
        'Conversation Id is not available',
        HttpStatus.FORBIDDEN,
      );
    }
  }

  @UseGuards(UserGuard)
  @Subscription(() => MessageOutput, {
    filter: (
      { newMessage }: { newMessage: MessageOutput },
      { conversation }: { conversation: string },
      { user }: { user: UserOutput },
    ) => {
      return (
        conversation === newMessage.conversation._id.toString() &&
        !!newMessage.conversation.participants.find(
          item => item._id.toString() === user._id,
        ) &&
        newMessage.sender._id.toString() !== user._id
      );
    },
  })
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  newMessage(@Args('conversation') conversation: string) {
    return this.pubSub.asyncIterator('newMessage');
  }

  @UseGuards(UserGuard)
  @Subscription(() => LastSeenOutput, {
    filter: (
      { newLastSeen }: { newLastSeen: LastSeenOutput },
      { conversation }: { conversation: string },
      { user }: { user: UserOutput },
    ) => {
      return (
        conversation === newLastSeen.conversation._id.toString() &&
        !!newLastSeen.conversation.participants.find(
          item => item._id.toString() === user._id,
        ) &&
        !!(newLastSeen.currentUser._id.toString() !== user._id)
      );
    },
  })
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  newLastSeen(@Args('conversation') conversation: string) {
    return this.pubSub.asyncIterator('newLastSeen');
  }
}
