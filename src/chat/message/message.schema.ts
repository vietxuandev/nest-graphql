import { Document, Types } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

import { Conversation } from 'src/chat/conversation/conversation.schema';
import { MessageStatus } from 'src/common/enums/message-status.enum';
import { User } from 'src/user/user.schema';
import { File } from 'src/file/file.schema';

@Schema({ timestamps: true })
export class Message extends Document {
  @Prop({ type: String, required: true })
  _id: string;

  @Prop({
    type: Types.ObjectId,
    ref: Conversation.name,
    required: true,
    autopopulate: true,
  })
  conversation: string;

  @Prop({
    type: Types.ObjectId,
    ref: Message.name,
    required: false,
    autopopulate: true,
  })
  message?: string;

  @Prop({
    type: Types.ObjectId,
    ref: User.name,
    required: true,
    autopopulate: true,
  })
  sender: string;

  @Prop({ type: String, required: false })
  content?: string;

  @Prop([
    {
      type: [Types.ObjectId],
      required: false,
      ref: File.name,
      autopopulate: true,
    },
  ])
  files?: File[];

  @Prop({ type: String, default: MessageStatus.SENT })
  status: string;
}

export const MessageSchema = SchemaFactory.createForClass(Message);
