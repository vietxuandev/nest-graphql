import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { MessageStatus } from 'src/common/enums/message-status.enum';
import { MessageSenderInput } from './dto/message.input';
import { Message } from './message.schema';
import { MessagesSearchService } from './messagesSearch.service';

@Injectable()
export class MessageService {
  constructor(
    @InjectModel(Message.name)
    private readonly messageModel: Model<Message>,
    private readonly messagesSearchService: MessagesSearchService,
  ) {}

  async create(input: MessageSenderInput): Promise<Message | undefined> {
    const createdMessage = new this.messageModel(input);
    return createdMessage.save();
  }
  async findByConversation(
    conversation: string,
    offset?: number,
    limit?: number,
  ): Promise<Message[]> {
    return this.messageModel
      .find({
        conversation,
      })
      .sort({ createdAt: -1 })
      .skip(offset)
      .limit(limit)
      .exec();
  }

  async findByIdAndUpdate(
    _id: string,
    status: MessageStatus,
  ): Promise<Message> {
    return this.messageModel.findByIdAndUpdate(_id, { status }).exec();
  }

  async updateManyStatus(
    currentUser: string,
    conversation: string,
  ): Promise<Message[]> {
    return this.messageModel
      .updateMany(
        {
          sender: {
            $ne: currentUser,
          },
          status: {
            $ne: MessageStatus.SEEN,
          },
          conversation,
        },
        { status: MessageStatus.SEEN },
      )
      .exec();
  }

  async searchForMessage(text: string) {
    const results = await this.messagesSearchService.search(text);
    const ids = results.map(result => result.id);
    if (!ids.length) {
      return [];
    }
    return this.messageModel.find().exec();
  }
}
