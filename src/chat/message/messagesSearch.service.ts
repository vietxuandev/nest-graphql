import { Injectable } from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { Message } from './message.schema';
import MessageSearchBody from './types/messageSearchBody.interface';
import MessageSearchResult from './types/messageSearchResult.interface';

@Injectable()
export class MessagesSearchService {
  index = 'messages';
  constructor(private readonly elasticsearchService: ElasticsearchService) {}

  async indexMessage(message: Message) {
    return this.elasticsearchService.index<
      MessageSearchResult,
      MessageSearchBody
    >({
      index: this.index,
      body: {
        id: message.id,
        content: message.content,
      },
    });
  }

  async search(text: string) {
    const { body } = await this.elasticsearchService.search<
      MessageSearchResult
    >({
      index: this.index,
      body: {
        query: {
          multi_match: {
            query: text,
            fields: ['content'],
          },
        },
      },
    });
    const hits = body.hits.hits;
    return hits.map(item => item._source);
  }
}
