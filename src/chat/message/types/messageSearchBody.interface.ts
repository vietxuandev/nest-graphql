export default interface MessageSearchBody {
  id: number;
  content: string;
}
