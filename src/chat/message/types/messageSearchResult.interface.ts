import MessageSearchBody from './messageSearchBody.interface';

export default interface MessageSearchResult {
  hits: {
    total: number;
    hits: Array<{
      _source: MessageSearchBody;
    }>;
  };
}
