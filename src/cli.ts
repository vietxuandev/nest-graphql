import { CommandModule, CommandService } from 'nestjs-command';

import { AppModule } from './app.module';
import { NestFactory } from '@nestjs/core';

async function bootstrap() {
  const app = await NestFactory.createApplicationContext(AppModule, {
    logger: ['error'], // only errors
  });
  app
    .select(CommandModule)
    .get(CommandService)
    .exec();
}
bootstrap();
