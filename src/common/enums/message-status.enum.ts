import { registerEnumType } from '@nestjs/graphql';

export enum MessageStatus {
  SENT = '0',
  SEEN = '1',
  REVOKE = '2',
  REMOVE = '3',
}

registerEnumType(MessageStatus, {
  name: 'MessageStatus',
});
