import { registerEnumType } from '@nestjs/graphql';

export enum AllowedRoles {
  ADMIN = 'admin',
  MANAGER = 'manager',
  CUSTOMER = 'customer',
}

registerEnumType(AllowedRoles, {
  name: 'AllowedRoles',
});
