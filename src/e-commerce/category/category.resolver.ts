import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { AllowedRoles } from 'src/common/enums/roles.enum';
import { CategoryInput } from './dto/category.input';
import { CategoryOutput } from './dto/category.output';
import { CategoryService } from './category.service';
import { Auth } from 'src/auth/auth.decorator';

@Resolver()
export class CategoryResolver {
  constructor(private readonly categoryService: CategoryService) {}

  @Query(() => [CategoryOutput])
  async categories(
    @Args('searchText', { type: () => String, nullable: true })
    searchText?: string,
  ) {
    if (searchText) {
      return await this.categoryService.findBySearchText(searchText);
    }
    return await this.categoryService.findAll();
  }

  @Mutation(() => CategoryOutput)
  @Auth(AllowedRoles.ADMIN, AllowedRoles.MANAGER)
  async createCategory(@Args('input') input: CategoryInput) {
    return await this.categoryService.create(input);
  }
}
