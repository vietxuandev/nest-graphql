import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Category } from './category.schema';
import { CategoryInput } from './dto/category.input';

@Injectable()
export class CategoryService {
  constructor(
    @InjectModel(Category.name)
    private readonly categoryModel: Model<Category>,
  ) {}

  async findById(id: string): Promise<Category | undefined> {
    return this.categoryModel.findById(id).exec();
  }

  async findAll(): Promise<Category[]> {
    return this.categoryModel.find().exec();
  }

  async findBySearchText(searchText: string): Promise<Category[]> {
    return this.categoryModel
      .find(
        { $text: { $search: searchText } },
        { score: { $meta: 'textScore' } },
      )
      .sort({ score: { $meta: 'textScore' } })
      .exec();
  }

  async create(input: CategoryInput): Promise<Category | undefined> {
    const createdCategory = new this.categoryModel(input);
    return createdCategory.save();
  }
}
