import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class CategoryOutput {
  @Field(() => ID, { nullable: true })
  _id?: string;

  @Field(() => String, { nullable: true })
  name?: string;
}
