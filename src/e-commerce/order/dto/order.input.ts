import { Field, ID, InputType, OmitType, PartialType } from '@nestjs/graphql';

@InputType()
export class ItemInput {
  @Field(() => ID, { nullable: false })
  product: string;

  @Field(() => Number, { nullable: false })
  quantity: number;
}

@InputType()
export class OrderBase {
  @Field(() => ID, { nullable: true })
  user?: string;

  @Field(() => [ItemInput], { nullable: false })
  items: ItemInput[];

  @Field(() => String, { nullable: false })
  address: string;

  @Field(() => String, { nullable: false })
  phoneNumber: string;

  @Field(() => String, { nullable: true })
  voucher?: string;
}

@InputType()
export class OrderUserInput extends PartialType(OrderBase) {}

@InputType()
export class OrderInput extends PartialType(OmitType(OrderBase, ['user'])) {}
