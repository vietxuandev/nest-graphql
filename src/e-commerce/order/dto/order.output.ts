import { Field, ID, ObjectType } from '@nestjs/graphql';
import { ProductOutput } from 'src/e-commerce/product/dto/product.output';
import { VoucherOutput } from 'src/e-commerce/voucher/dto/voucher.output';
import { UserOutput } from 'src/user/dto/user.output';

@ObjectType()
export class ItemOutput {
  @Field(() => ProductOutput, { nullable: true })
  product?: ProductOutput;

  @Field(() => Number, { nullable: true })
  quantity?: number;
}

@ObjectType()
export class OrderOutput {
  @Field(() => ID, { nullable: true })
  _id?: string;

  @Field(() => UserOutput, { nullable: true })
  user?: UserOutput;

  @Field(() => [ItemOutput], { nullable: true })
  items?: ItemOutput[];

  @Field(() => String, { nullable: true })
  address?: string;

  @Field(() => String, { nullable: true })
  phoneNumber?: string;

  @Field(() => VoucherOutput, { nullable: true })
  voucher?: UserOutput;
}
