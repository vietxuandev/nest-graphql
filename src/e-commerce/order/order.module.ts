import { Order, OrderSchema } from './order.schema';

import AutoPopulate from 'mongoose-autopopulate';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { OrderResolver } from './order.resolver';
import { OrderService } from './order.service';
import { UserModule } from 'src/user/user.module';
import { VoucherModule } from '../voucher/voucher.module';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: Order.name,
        useFactory: () => {
          const schema = OrderSchema;
          schema.plugin(AutoPopulate);
          return schema;
        },
      },
    ]),
    UserModule,
    VoucherModule,
  ],
  providers: [OrderService, OrderResolver],
  exports: [OrderService],
})
export class OrderModule {}
