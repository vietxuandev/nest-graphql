import { OrderService } from './order.service';
import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { OrderInput } from './dto/order.input';
import { HttpException, HttpStatus, UseGuards } from '@nestjs/common';
import { CurrentUser } from 'src/user/user.decorator';
import { User } from 'src/user/user.schema';
import { UserGuard } from 'src/user/user.guard';
import { OrderOutput } from './dto/order.output';
import { UserService } from 'src/user/user.service';
import { VoucherService } from '../voucher/voucher.service';

@Resolver()
export class OrderResolver {
  constructor(
    private readonly orderService: OrderService,
    private readonly userService: UserService,
    private readonly voucherService: VoucherService,
  ) {}

  @Mutation(() => OrderOutput)
  @UseGuards(UserGuard)
  async createOrder(
    @Args('input') input: OrderInput,
    @CurrentUser() currentUser: User,
  ) {
    const user = currentUser
      ? await this.userService.findById(currentUser._id)
      : null;

    const voucher = input.voucher
      ? await this.voucherService.findByCode(input.voucher)
      : null;
    if (voucher) {
      if (voucher.available) {
        await this.voucherService.findByIdAndUpdate(voucher._id);
      } else {
        throw new HttpException(
          'Voucher is not available',
          HttpStatus.FORBIDDEN,
        );
      }
    }
    const dataInput = {
      ...input,
      user: user ? user._id : null,
      voucher: voucher ? voucher._id : null,
    };
    return await this.orderService.create(dataInput);
  }
}
