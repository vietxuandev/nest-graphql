import { Document, Types } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { User } from 'src/user/user.schema';
import { Product } from '../product/product.schema';
import { Voucher } from '../voucher/voucher.schema';

@Schema({ timestamps: true })
export class Order extends Document {
  @Prop({
    type: Types.ObjectId,
    ref: User.name,
    required: false,
    autopopulate: true,
  })
  user?: User;

  @Prop([
    {
      product: {
        type: Types.ObjectId,
        ref: Product.name,
        required: true,
        autopopulate: true,
      },
      quantity: Number,
    },
  ])
  items: { product: Product; quantity: number };

  @Prop({ type: String, required: true })
  address: string;

  @Prop({ type: String, required: true })
  phoneNumber: string;

  @Prop({
    type: Types.ObjectId,
    ref: Voucher.name,
    required: false,
    autopopulate: true,
  })
  voucher?: Voucher;
}

export const OrderSchema = SchemaFactory.createForClass(Order);
