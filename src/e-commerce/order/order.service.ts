import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { OrderUserInput } from './dto/order.input';
import { Order } from './order.schema';

@Injectable()
export class OrderService {
  constructor(
    @InjectModel(Order.name)
    private readonly orderModel: Model<Order>,
  ) {}

  async findAndUpdate(
    filter: Record<string, unknown>,
    update: Record<string, unknown>,
  ): Promise<void> {
    this.orderModel.updateMany(filter, update, { upsert: true }).exec();
  }

  async create(input: OrderUserInput): Promise<Order | undefined> {
    return new this.orderModel(input).save();
  }
}
