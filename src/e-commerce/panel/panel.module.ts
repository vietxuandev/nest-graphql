import { Module } from '@nestjs/common';
import { PanelResolver } from './panel.resolver';
import { PanelService } from './panel.service';

@Module({
  providers: [PanelResolver, PanelService]
})
export class PanelModule {}
