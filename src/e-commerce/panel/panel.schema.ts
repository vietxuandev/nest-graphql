import { Document, Types } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { File } from 'src/file/file.schema';

@Schema({ timestamps: true })
export class Panel extends Document {
  @Prop({ type: String, required: false })
  title: string;

  @Prop({
    type: Types.ObjectId,
    required: false,
    ref: File.name,
    autopopulate: true,
  })
  image?: File;

  @Prop({ type: String, required: false })
  description: string;
}

export const PanelSchema = SchemaFactory.createForClass(Panel);
