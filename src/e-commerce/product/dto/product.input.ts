import { Field, ID, InputType } from '@nestjs/graphql';
import { GraphQLUpload } from 'graphql-upload';

@InputType()
export class ProducrInput {
  @Field(() => String, { nullable: false })
  name: string;

  @Field(() => Number, { nullable: false })
  price: number;

  @Field(() => Number, { nullable: true })
  discount: number;

  @Field(() => ID, { nullable: false })
  category: string;

  @Field(() => [GraphQLUpload], { nullable: true })
  images?: File[];

  @Field(() => String, { nullable: true })
  description?: string;
}
