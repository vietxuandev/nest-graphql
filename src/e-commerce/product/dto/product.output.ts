import { Field, ID, ObjectType } from '@nestjs/graphql';
import { CategoryOutput } from 'src/e-commerce/category/dto/category.output';
import { FileOutput } from 'src/file/dto/file.output';

@ObjectType()
export class ProductOutput {
  @Field(() => ID, { nullable: true })
  _id?: string;

  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => Number, { nullable: true })
  price?: number;

  @Field(() => Number, { nullable: true })
  discount?: number;

  @Field(() => CategoryOutput, { nullable: true })
  category?: CategoryOutput;

  @Field(() => [FileOutput], { nullable: true })
  images?: FileOutput[];

  @Field(() => String, { nullable: true })
  description?: string;
}
