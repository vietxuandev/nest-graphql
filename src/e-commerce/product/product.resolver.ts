import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { ProductService } from './product.service';
import { PubSub } from 'graphql-subscriptions';
import { HttpException, HttpStatus, UseGuards } from '@nestjs/common';
import { AllowedRoles } from 'src/common/enums/roles.enum';
import { Auth } from 'src/auth/auth.decorator';
import { ProductOutput } from './dto/product.output';
import { ProducrInput } from './dto/product.input';
import { UserGuard } from 'src/user/user.guard';
import { CategoryService } from '../category/category.service';

@Resolver()
export class ProductResolver {
  private pubSub: PubSub;
  constructor(
    private readonly productService: ProductService,
    private readonly categoryService: CategoryService,
  ) {
    this.pubSub = new PubSub();
  }

  @Query(() => [ProductOutput])
  async products(
    @Args('searchText', { type: () => String, nullable: true })
    searchText?: string,
  ) {
    if (searchText) {
      return await this.productService.findBySearchText(searchText);
    }
    return await this.productService.findAll();
  }

  @Mutation(() => ProductOutput)
  @Auth(AllowedRoles.ADMIN, AllowedRoles.MANAGER)
  async createProduct(@Args('input') input: ProducrInput) {
    const product = await this.productService.findByName(input.name);
    if (!product) {
      const category = await this.categoryService.findById(input.category);
      const newProduct = await this.productService.create({
        ...input,
        category: category._id,
      });
      this.pubSub.publish('newProduct', { newProduct });
      return newProduct;
    }
    throw new HttpException('This product already exists', HttpStatus.CONFLICT);
  }

  @UseGuards(UserGuard)
  @Subscription(() => ProductOutput, {
    filter: (payload, variables, context) => {
      console.log(context.user, payload, variables);
      return true;
    },
  })
  newProduct() {
    return this.pubSub.asyncIterator('newProduct');
  }
}
