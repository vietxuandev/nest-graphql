import { Document, Types } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Category } from '../category/category.schema';
import { File } from 'src/file/file.schema';

@Schema({ timestamps: true })
export class Product extends Document {
  @Prop({ type: String, required: true })
  name: string;

  @Prop({ type: Number, required: true })
  price: string;

  @Prop({ type: Number, required: false, default: 0 })
  discount: number;

  @Prop({ type: Types.ObjectId, ref: Category.name, autopopulate: true })
  category: Category;

  @Prop([
    {
      type: [Types.ObjectId],
      required: false,
      ref: File.name,
      autopopulate: true,
    },
  ])
  images?: File[];

  @Prop({ type: String, required: false })
  description: string;
}

export const ProductSchema = SchemaFactory.createForClass(Product);
