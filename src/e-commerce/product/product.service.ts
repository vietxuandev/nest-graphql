import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Product } from './product.schema';
import { ProducrInput } from './dto/product.input';

@Injectable()
export class ProductService {
  constructor(
    @InjectModel(Product.name)
    private readonly productModel: Model<Product>,
  ) {}

  async findById(id: string): Promise<Product | undefined> {
    return this.productModel.findById(id).exec();
  }

  async findByName(name: string): Promise<Product | undefined> {
    return this.productModel.findOne({ name }).exec();
  }

  async findBySearchText(searchText: string): Promise<Product[]> {
    return this.productModel
      .find(
        { $text: { $search: searchText } },
        { score: { $meta: 'textScore' } },
      )
      .sort({ score: { $meta: 'textScore' } })
      .exec();
  }

  async findAll(): Promise<Product[]> {
    return this.productModel.find().exec();
  }

  async create(input: ProducrInput): Promise<Product | undefined> {
    return new this.productModel(input).save();
  }
}
