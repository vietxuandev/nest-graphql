import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class VoucherInput {
  @Field(() => String, { nullable: false })
  code: string;

  @Field(() => Number, { nullable: false })
  percentage: number;

  @Field(() => Boolean, { nullable: true })
  available?: boolean;
}
