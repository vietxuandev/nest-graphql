import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class VoucherOutput {
  @Field(() => ID, { nullable: true })
  _id?: string;

  @Field(() => String, { nullable: true })
  code?: string;

  @Field(() => Number, { nullable: true })
  percentage?: number;

  @Field(() => Boolean, { nullable: true })
  available?: boolean;
}
