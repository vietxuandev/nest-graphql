import { Voucher, VoucherSchema } from './voucher.schema';

import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { VoucherResolver } from './voucher.resolver';
import { VoucherSeed } from './voucher.seed';
import { VoucherService } from './voucher.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Voucher.name,
        schema: VoucherSchema,
      },
    ]),
  ],
  providers: [VoucherService, VoucherResolver, VoucherSeed],
  exports: [VoucherService, VoucherSeed],
})
export class VoucherModule {}
