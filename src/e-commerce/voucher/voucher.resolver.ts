import { Args, Query, Resolver } from '@nestjs/graphql';

import { VoucherOutput } from './dto/voucher.output';
import { VoucherService } from './voucher.service';

@Resolver()
export class VoucherResolver {
  constructor(private readonly voucherService: VoucherService) {}
  @Query(() => [VoucherOutput])
  async vouchers() {
    return await this.voucherService.findAll();
  }
  @Query(() => [VoucherOutput])
  async availableVouchers() {
    return await this.voucherService.findAvailableVouchers();
  }

  @Query(() => Boolean)
  async checkAvailableVoucher(
    @Args('code', { type: () => String, nullable: true }) code: string,
  ) {
    return (await this.voucherService.findByCode(code)).available;
  }
}
