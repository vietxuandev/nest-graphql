import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

import { Document } from 'mongoose';

@Schema({ timestamps: true })
export class Voucher extends Document {
  @Prop({ type: String, required: true })
  code: string;

  @Prop({ type: Number, required: true })
  percentage: number;

  @Prop({ type: Boolean, required: false, default: true })
  available: boolean;
}

export const VoucherSchema = SchemaFactory.createForClass(Voucher);
