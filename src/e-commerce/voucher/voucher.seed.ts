import { Command } from 'nestjs-command';
import { Injectable } from '@nestjs/common';
import { VoucherService } from './voucher.service';
import generator from 'voucher-code-generator';
import readline from 'readline';

@Injectable()
export class VoucherSeed {
  constructor(private readonly voucherService: VoucherService) {}

  @Command({
    command: 'create:vouchers',
    describe: 'Create Vouchers',
    autoExit: true,
  })
  async create() {
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });

    function question(theQuestion: string) {
      return new Promise<string>(resolve =>
        rl.question(theQuestion, answ => resolve(answ)),
      );
    }

    const count = await question('Enter the number of discount codes: ');
    const prefix = await question('Enter the prefix code: ');
    const percentage = await question('Enter the percentage value: ');
    const voucherCodes = generator.generate({
      length: 7,
      count: Number(count),
      prefix: prefix.toUpperCase(),
    });
    for await (const code of voucherCodes) {
      await this.voucherService.create({
        code,
        percentage: Number(percentage),
      });
    }
    console.log(`${count} voucher is created!`);
    console.log('Detail:', voucherCodes, `Percentage: ${percentage}%`);
  }
}
