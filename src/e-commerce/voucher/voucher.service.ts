import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { VoucherInput } from './dto/voucher.input';
import { Voucher } from './voucher.schema';

@Injectable()
export class VoucherService {
  constructor(
    @InjectModel(Voucher.name)
    private readonly voucherModel: Model<Voucher>,
  ) {}

  async findAll(): Promise<Voucher[]> {
    return this.voucherModel.find().exec();
  }

  async findByCode(code: string): Promise<Voucher> {
    return this.voucherModel.findOne({ code }).exec();
  }

  async findByIdAndUpdate(id: string): Promise<Voucher> {
    return this.voucherModel
      .findByIdAndUpdate(id, { available: false }, { new: true })
      .exec();
  }

  async findAvailableVouchers(): Promise<Voucher[]> {
    return this.voucherModel.find({ available: true }).exec();
  }

  async create(input: VoucherInput): Promise<Voucher | undefined> {
    return new this.voucherModel(input).save();
  }
}
