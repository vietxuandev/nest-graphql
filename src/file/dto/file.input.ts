import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class FileInput {
  @Field(() => String)
  filename: string;
  @Field(() => String)
  mimetype: string;
  @Field(() => String)
  src: string;
}
