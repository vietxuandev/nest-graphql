import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class FileOutput {
  @Field(() => ID, { nullable: true })
  _id?: string;
  @Field(() => String, { nullable: true })
  filename?: string;
  @Field(() => String, { nullable: true })
  mimetype?: string;
  @Field(() => String, { nullable: true })
  src?: string;
}
