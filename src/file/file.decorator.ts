import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';

export const UploadedFiles = createParamDecorator(
  (data: string, context: ExecutionContext) => {
    const ctx = GqlExecutionContext.create(context);
    if (ctx.getContext()?.locals) {
      const { uploadedFiles } = ctx.getContext().locals;
      return uploadedFiles;
    }
    return [];
  },
);
