import {
  CallHandler,
  ExecutionContext,
  Injectable,
  mixin,
  NestInterceptor,
} from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { FileUpload } from 'graphql-upload';
import { Observable } from 'rxjs';
import { FileService } from './file.service';

export function GqlFileInterceptor(fieldNameList: string[]) {
  @Injectable()
  class FileInterceptor implements NestInterceptor {
    constructor(private readonly fileService: FileService) {}
    async upload(fieldName: string, gqlCtx: GqlExecutionContext) {
      const { uploadedFiles } = gqlCtx.getContext().locals;
      const files = await gqlCtx.getArgs().input[fieldName];
      return Promise.all(
        (Array.isArray(files) ? files : [files]).map(
          async (file: Promise<FileUpload>) => {
            return this.fileService.storeFile(file).then(address => {
              if (!uploadedFiles[fieldName]) {
                uploadedFiles[fieldName] = [address];
              } else {
                uploadedFiles[fieldName].push(address);
              }
              return address;
            });
          },
        ),
      );
    }
    async intercept(
      context: ExecutionContext,
      next: CallHandler,
    ): Promise<Observable<any>> {
      const gqlCtx = GqlExecutionContext.create(context);
      const existFieldNameInArgs = fieldNameList.some(fieldName =>
        Object.keys(gqlCtx.getArgs().input).includes(fieldName),
      );
      if (existFieldNameInArgs) {
        gqlCtx.getContext().locals = {
          uploadedFiles: {},
        };
        await Promise.all(
          fieldNameList.map(fieldName => this.upload(fieldName, gqlCtx)),
        );
      }
      return next.handle();
    }
  }
  return mixin(FileInterceptor);
}
