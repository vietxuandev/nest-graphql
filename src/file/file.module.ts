import { Module } from '@nestjs/common';
import AutoPopulate from 'mongoose-autopopulate';
import { MongooseModule } from '@nestjs/mongoose';
import { FileResolver } from './file.resolver';
import { File, FileSchema } from './file.schema';
import { FileService } from './file.service';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: File.name,
        useFactory: () => {
          const schema = FileSchema;
          schema.plugin(AutoPopulate);
          return schema;
        },
      },
    ]),
  ],
  providers: [FileResolver, FileService],
  exports: [FileService],
})
export class FileModule {}
