import { Injectable } from '@nestjs/common';
import { v4 as uuid } from 'uuid';
import path from 'path';
import { FileUpload } from 'graphql-upload';
import fs, { createWriteStream } from 'fs';
import { InjectModel } from '@nestjs/mongoose';
import { File } from './file.schema';
import { Model } from 'mongoose';
import { FileInput } from './dto/file.input';

const imageDir = './public/images';
if (!fs.existsSync(imageDir)) {
  fs.mkdirSync(imageDir);
}

const fileDir = './public/files';
if (!fs.existsSync(fileDir)) {
  fs.mkdirSync(fileDir);
}

@Injectable()
export class FileService {
  constructor(
    @InjectModel(File.name)
    private readonly fileModel: Model<File>,
  ) {}
  async create(input: FileInput): Promise<File | undefined> {
    const createdFile = new this.fileModel(input);
    return createdFile.save();
  }
  async storeFile(file: Promise<FileUpload>): Promise<File> {
    const { createReadStream, filename, mimetype } = await file;
    const isImage = mimetype.includes('image');
    const id = uuid();
    const src = path.join(isImage ? imageDir : fileDir, `${id}-${filename}`);
    return new Promise((resolve, reject) =>
      createReadStream()
        .pipe(createWriteStream(src))
        .on('finish', async () => {
          const file = await this.create({
            filename,
            mimetype,
            src: src.slice(6),
          });
          resolve(file);
        })
        .on('error', error => reject(error)),
    );
  }
}
