import { Injectable } from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { City } from './city.schema';
import CitySearchBody from './types/citySearchBody.interface';
import CitySearchResult from './types/citySearchResult.interface';

@Injectable()
export class CitiesSearchService {
  index = 'cities';
  constructor(private readonly elasticsearchService: ElasticsearchService) {}

  async indexCity(city: City) {
    return this.elasticsearchService.index<CitySearchResult, CitySearchBody>({
      index: this.index,
      body: {
        id: city.id,
        name: city.name,
        code: city.code,
      },
    });
  }

  async search(text: string) {
    const { body } = await this.elasticsearchService.search<CitySearchResult>({
      index: this.index,
      body: {
        query: {
          multi_match: {
            query: text,
            fields: ['name', 'code'],
          },
        },
      },
    });
    const hits = body.hits.hits;
    return hits.map(item => item._source);
  }
}
