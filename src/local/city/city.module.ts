import { City, CitySchema } from './city.schema';

import AutoPopulate from 'mongoose-autopopulate';
import { CityResolver } from './city.resolver';
import { CitySeed } from './city.seed';
import { CityService } from './city.service';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { WardModule } from 'src/local/ward/ward.module';
import { SearchModule } from 'src/search/search.module';
import { CitiesSearchService } from './citiesSearch.service';
import { StreetModule } from '../street/street.module';
import { DistrictModule } from '../district/district.module';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: City.name,
        useFactory: () => {
          const schema = CitySchema;
          schema.plugin(AutoPopulate);
          return schema;
        },
      },
    ]),
    DistrictModule,
    StreetModule,
    WardModule,
    SearchModule,
  ],
  providers: [CityService, CitiesSearchService, CitySeed, CityResolver],
  exports: [CitySeed, CitiesSearchService],
})
export class CityModule {}
