import { Args, Query, Resolver } from '@nestjs/graphql';
import { CityOutput } from './dto/city.output';
import { CityService } from './city.service';

@Resolver()
export class CityResolver {
  constructor(private readonly cityService: CityService) {}

  @Query(() => [CityOutput])
  async cities(
    @Args('searchText', { type: () => String, nullable: true })
    searchText?: string,
  ) {
    if (searchText) {
      return await this.cityService.searchForCity(searchText);
    }
    return await this.cityService.findAll();
  }
}
