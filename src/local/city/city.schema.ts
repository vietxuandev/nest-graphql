import { Document, Types } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { District } from '../district/district.schema';

@Schema({ timestamps: true })
export class City extends Document {
  @Prop({ type: String, required: true })
  code: string;

  @Prop({ type: String, required: true })
  name: string;

  @Prop([
    {
      type: [Types.ObjectId],
      required: true,
      ref: District.name,
      autopopulate: true,
    },
  ])
  districts: District[];
}

export const CitySchema = SchemaFactory.createForClass(City);
