import { CityService } from './city.service';
import { Command } from 'nestjs-command';
import { DistrictService } from '../district/district.service';
import { Injectable } from '@nestjs/common';
import Local from 'src/local.json';
import { StreetService } from '../street/street.service';
import { WardService } from '../ward/ward.service';

@Injectable()
export class CitySeed {
  constructor(
    private readonly districtService: DistrictService,
    private readonly streetService: StreetService,
    private readonly cityService: CityService,
    private readonly wardService: WardService,
  ) {}

  @Command({
    command: 'create:local',
    describe: 'Create local',
    autoExit: true,
  })
  async create() {
    const cities = await this.cityService.findAll();
    if (!cities.length) {
      for await (const city of Local) {
        const districts = [];
        for await (const district of city.districts) {
          const wards = [];
          for await (const ward of district.wards) {
            const newWard = await this.wardService.create({
              name: ward.name,
              prefix: ward.prefix,
            });
            wards.push(newWard);
          }
          const streets = [];
          for await (const street of district.streets) {
            const newStreet = await this.streetService.create({
              name: street.name,
              prefix: street.prefix,
            });
            streets.push(newStreet);
          }
          const newDistrict = await this.districtService.create({
            name: district.name,
            wards,
            streets,
          });
          districts.push(newDistrict);
        }
        await this.cityService.create({
          code: city.code,
          name: city.name,
          districts,
        });
      }
      console.log('Local was created');
    } else {
      console.log('Local already exists');
    }
  }
}
