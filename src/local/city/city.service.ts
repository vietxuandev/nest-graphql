import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CityInput } from './dto/city.input';
import { City } from './city.schema';
import { CitiesSearchService } from './citiesSearch.service';

@Injectable()
export class CityService {
  constructor(
    @InjectModel(City.name)
    private readonly cityModel: Model<City>,
    private readonly citiesSearchService: CitiesSearchService,
  ) {}
  async findAll(): Promise<City[]> {
    return this.cityModel.find().exec();
  }

  async find(id: string): Promise<City | undefined> {
    const createdCity = this.cityModel.findById(id).exec();
    return createdCity;
  }
  async create(input: CityInput): Promise<City | undefined> {
    const createdCity = new this.cityModel(input);
    this.citiesSearchService.indexCity(createdCity);
    return createdCity.save();
  }
  async searchForCity(text: string) {
    const results = await this.citiesSearchService.search(text);
    const ids = results.map(result => result.id);
    if (!ids.length) {
      return [];
    }
    return this.cityModel.find({ _id: { $in: ids } }).exec();
  }
}
