import { Field, ID, InputType } from '@nestjs/graphql';

@InputType()
export class CityInput {
  @Field(() => String, { nullable: false })
  code: string;

  @Field(() => String, { nullable: false })
  name: string;

  @Field(() => [ID], { nullable: false })
  districts: string[];
}
