import { Field, ID, ObjectType } from '@nestjs/graphql';
import { DistrictOutput } from 'src/local/district/dto/district.output';

@ObjectType()
export class CityOutput {
  @Field(() => ID, { nullable: true })
  _id?: string;

  @Field(() => String, { nullable: true })
  code?: string;

  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => [DistrictOutput], { nullable: true })
  districts?: DistrictOutput[];
}
