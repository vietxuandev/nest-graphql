export default interface CitySearchBody {
  id: number;
  name: string;
  code: string;
}
