import CitySearchBody from './citySearchBody.interface';

export default interface CitySearchResult {
  hits: {
    total: number;
    hits: Array<{
      _source: CitySearchBody;
    }>;
  };
}
