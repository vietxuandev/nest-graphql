import { District, DistrictSchema } from './district.schema';

import AutoPopulate from 'mongoose-autopopulate';
import { DistrictResolver } from './district.resolver';
import { DistrictService } from './district.service';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SearchModule } from 'src/search/search.module';
import { DistrictsSearchService } from './districtsSearch.service';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: District.name,
        useFactory: () => {
          const schema = DistrictSchema;
          schema.plugin(AutoPopulate);
          return schema;
        },
      },
    ]),
    SearchModule,
  ],
  providers: [DistrictService, DistrictsSearchService, DistrictResolver],
  exports: [DistrictService, DistrictsSearchService],
})
export class DistrictModule {}
