import { Args, Query, Resolver } from '@nestjs/graphql';
import { DistrictOutput } from './dto/district.output';
import { DistrictService } from './district.service';

@Resolver()
export class DistrictResolver {
  constructor(private readonly districtService: DistrictService) {}

  @Query(() => [DistrictOutput])
  async districts(
    @Args('searchText', { type: () => String, nullable: true })
    searchText?: string,
  ) {
    if (searchText) {
      return await this.districtService.searchForDistrict(searchText);
    }
    return await this.districtService.findAll();
  }
}
