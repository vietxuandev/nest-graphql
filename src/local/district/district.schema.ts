import { Document, Types } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Ward } from '../ward/ward.schema';
import { Street } from '../street/street.schema';

@Schema({ timestamps: true })
export class District extends Document {
  @Prop({ type: String, required: true })
  name: string;

  @Prop([
    {
      type: [Types.ObjectId],
      required: true,
      ref: Ward.name,
      autopopulate: true,
    },
  ])
  wards: Ward[];

  @Prop([
    {
      type: [Types.ObjectId],
      required: true,
      ref: Street.name,
      autopopulate: true,
    },
  ])
  streets: Street[];
}

export const DistrictSchema = SchemaFactory.createForClass(District);
