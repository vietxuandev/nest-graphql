import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { DistrictInput } from './dto/district.input';
import { District } from './district.schema';
import { DistrictsSearchService } from './districtsSearch.service';

@Injectable()
export class DistrictService {
  constructor(
    @InjectModel(District.name)
    private readonly districtModel: Model<District>,
    private readonly districtsSearchService: DistrictsSearchService,
  ) {}

  async findAll(): Promise<District[]> {
    return this.districtModel.find().exec();
  }

  async findById(id: string): Promise<District | undefined> {
    const district = this.districtModel.findById(id).exec();
    return district;
  }
  async create(input: DistrictInput): Promise<District | undefined> {
    const createdDistrict = new this.districtModel(input);
    this.districtsSearchService.indexDistrict(createdDistrict);
    return createdDistrict.save();
  }
  async searchForDistrict(text: string) {
    const results = await this.districtsSearchService.search(text);
    const ids = results.map(result => result.id);
    if (!ids.length) {
      return [];
    }
    return this.districtModel.find({ _id: { $in: ids } }).exec();
  }
}
