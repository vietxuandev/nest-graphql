import { Injectable } from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { District } from './district.schema';
import DistrictSearchBody from './types/districtSearchBody.interface';
import DistrictSearchResult from './types/districtSearchResult.interface';

@Injectable()
export class DistrictsSearchService {
  index = 'districts';
  constructor(private readonly elasticsearchService: ElasticsearchService) {}

  async indexDistrict(district: District) {
    return this.elasticsearchService.index<
      DistrictSearchResult,
      DistrictSearchBody
    >({
      index: this.index,
      body: {
        id: district.id,
        name: district.name,
      },
    });
  }

  async search(text: string) {
    const { body } = await this.elasticsearchService.search<
      DistrictSearchResult
    >({
      index: this.index,
      body: {
        query: {
          multi_match: {
            query: text,
            fields: ['name'],
          },
        },
      },
    });
    const hits = body.hits.hits;
    return hits.map(item => item._source);
  }
}
