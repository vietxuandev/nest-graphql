import { Field, ID, InputType } from '@nestjs/graphql';

@InputType()
export class DistrictInput {
  @Field(() => String, { nullable: false })
  name: string;

  @Field(() => [ID], { nullable: false })
  wards: string[];

  @Field(() => [ID], { nullable: false })
  streets: string[];
}
