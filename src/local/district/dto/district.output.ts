import { Field, ID, ObjectType } from '@nestjs/graphql';
import { StreetOutput } from 'src/local/street/dto/street.output';
import { WardOutput } from 'src/local/ward/dto/ward.output';

@ObjectType()
export class DistrictOutput {
  @Field(() => ID, { nullable: true })
  _id?: string;

  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => [WardOutput], { nullable: true })
  wards?: WardOutput[];

  @Field(() => [StreetOutput], { nullable: true })
  streets?: StreetOutput[];
}
