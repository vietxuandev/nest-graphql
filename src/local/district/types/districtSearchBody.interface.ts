export default interface DistrictSearchBody {
  id: number;
  name: string;
}
