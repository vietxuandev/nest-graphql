import DistrictSearchBody from './districtSearchBody.interface';

export default interface DistrictSearchResult {
  hits: {
    total: number;
    hits: Array<{
      _source: DistrictSearchBody;
    }>;
  };
}
