import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class StreetInput {
  @Field(() => String, { nullable: false })
  name: string;

  @Field(() => String, { nullable: false })
  prefix: string;
}
