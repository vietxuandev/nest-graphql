import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class StreetOutput {
  @Field(() => ID, { nullable: true })
  _id?: string;

  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => String, { nullable: true })
  prefix?: string;
}
