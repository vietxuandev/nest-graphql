import { Street, StreetSchema } from './street.schema';

import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { StreetResolver } from './street.resolver';
import { StreetService } from './street.service';
import { StreetsSearchService } from './streetsSearch.service';
import { SearchModule } from 'src/search/search.module';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: Street.name,
        useFactory: () => {
          const schema = StreetSchema;
          return schema;
        },
      },
    ]),
    SearchModule,
  ],
  providers: [StreetService, StreetsSearchService, StreetResolver],
  exports: [StreetService, StreetsSearchService],
})
export class StreetModule {}
