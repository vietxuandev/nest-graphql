import { Test, TestingModule } from '@nestjs/testing';
import { StreetResolver } from './street.resolver';

describe('StreetResolver', () => {
  let resolver: StreetResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StreetResolver],
    }).compile();

    resolver = module.get<StreetResolver>(StreetResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
