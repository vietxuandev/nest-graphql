import { Args, Query, Resolver } from '@nestjs/graphql';
import { StreetOutput } from './dto/street.output';
import { StreetService } from './street.service';

@Resolver()
export class StreetResolver {
  constructor(private readonly streetService: StreetService) {}

  @Query(() => [StreetOutput])
  async streets(
    @Args('searchText', { type: () => String, nullable: true })
    searchText?: string,
  ) {
    if (searchText) {
      return await this.streetService.searchForStreet(searchText);
    }
    return await this.streetService.findAll();
  }
}
