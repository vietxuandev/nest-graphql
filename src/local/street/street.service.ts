import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { StreetInput } from './dto/street.input';
import { Street } from './street.schema';
import { StreetsSearchService } from './streetsSearch.service';

@Injectable()
export class StreetService {
  constructor(
    @InjectModel(Street.name)
    private readonly streetModel: Model<Street>,
    private readonly streetsSearchService: StreetsSearchService,
  ) {}

  async findAll(): Promise<Street[]> {
    return this.streetModel.find().exec();
  }

  async findById(id: string): Promise<Street | undefined> {
    const street = this.streetModel.findById(id).exec();
    return street;
  }
  async create(input: StreetInput): Promise<Street | undefined> {
    const createdStreet = new this.streetModel(input);
    this.streetsSearchService.indexStreet(createdStreet);
    return createdStreet.save();
  }
  async searchForStreet(text: string) {
    const results = await this.streetsSearchService.search(text);
    const ids = results.map(result => result.id);
    if (!ids.length) {
      return [];
    }
    return this.streetModel.find({ _id: { $in: ids } }).exec();
  }
}
