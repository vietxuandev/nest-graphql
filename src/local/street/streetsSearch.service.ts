import { Injectable } from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { Street } from './street.schema';
import StreetSearchBody from './types/streetSearchBody.interface';
import StreetSearchResult from './types/streetSearchResult.interface';

@Injectable()
export class StreetsSearchService {
  index = 'streets';
  constructor(private readonly elasticsearchService: ElasticsearchService) {}

  async indexStreet(street: Street) {
    return this.elasticsearchService.index<
      StreetSearchResult,
      StreetSearchBody
    >({
      index: this.index,
      body: {
        id: street.id,
        name: street.name,
        prefix: street.prefix,
      },
    });
  }

  async search(text: string) {
    const { body } = await this.elasticsearchService.search<StreetSearchResult>(
      {
        index: this.index,
        body: {
          query: {
            multi_match: {
              query: text,
              fields: ['name', 'prefix'],
            },
          },
        },
      },
    );
    const hits = body.hits.hits;
    return hits.map(item => item._source);
  }
}
