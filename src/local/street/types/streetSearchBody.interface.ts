export default interface StreetSearchBody {
  id: number;
  name: string;
  prefix: string;
}
