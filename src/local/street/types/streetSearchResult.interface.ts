import StreetSearchBody from './streetSearchBody.interface';

export default interface StreetSearchResult {
  hits: {
    total: number;
    hits: Array<{
      _source: StreetSearchBody;
    }>;
  };
}
