import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class WardInput {
  @Field(() => String, { nullable: false })
  name: string;

  @Field(() => String, { nullable: false })
  prefix: string;
}
