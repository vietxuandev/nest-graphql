export default interface WardSearchBody {
  id: number;
  name: string;
  prefix: string;
}
