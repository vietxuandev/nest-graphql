import WardSearchBody from './wardSearchBody.interface';

export default interface WardSearchResult {
  hits: {
    total: number;
    hits: Array<{
      _source: WardSearchBody;
    }>;
  };
}
