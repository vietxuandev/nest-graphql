import { Ward, WardSchema } from './ward.schema';

import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { WardResolver } from './ward.resolver';
import { WardService } from './ward.service';
import { SearchModule } from 'src/search/search.module';
import { WardsSearchService } from './wardsSearch.service';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: Ward.name,
        useFactory: () => {
          const schema = WardSchema;
          return schema;
        },
      },
    ]),
    SearchModule,
  ],
  providers: [WardService, WardsSearchService, WardResolver],
  exports: [WardService, WardsSearchService],
})
export class WardModule {}
