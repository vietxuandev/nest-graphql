import { Test, TestingModule } from '@nestjs/testing';
import { WardResolver } from './ward.resolver';

describe('WardResolver', () => {
  let resolver: WardResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [WardResolver],
    }).compile();

    resolver = module.get<WardResolver>(WardResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
