import { Args, Query, Resolver } from '@nestjs/graphql';
import { WardOutput } from './dto/ward.output';
import { WardService } from './ward.service';

@Resolver()
export class WardResolver {
  constructor(private readonly wardService: WardService) {}

  @Query(() => [WardOutput])
  async wards(
    @Args('searchText', { type: () => String, nullable: true })
    searchText?: string,
  ) {
    if (searchText) {
      return await this.wardService.searchForWard(searchText);
    }
    return await this.wardService.findAll();
  }
}
