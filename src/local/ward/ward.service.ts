import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { WardInput } from './dto/ward.input';
import { Ward } from './ward.schema';
import { WardsSearchService } from './wardsSearch.service';

@Injectable()
export class WardService {
  constructor(
    @InjectModel(Ward.name)
    private readonly wardModel: Model<Ward>,
    private readonly wardsSearchService: WardsSearchService,
  ) {}

  async findAll(): Promise<Ward[]> {
    return this.wardModel.find().exec();
  }

  async findById(id: string): Promise<Ward | undefined> {
    const ward = this.wardModel.findById(id).exec();
    return ward;
  }
  async create(input: WardInput): Promise<Ward | undefined> {
    const createdWard = new this.wardModel(input);
    this.wardsSearchService.indexWard(createdWard);
    return createdWard.save();
  }
  async searchForWard(text: string) {
    const results = await this.wardsSearchService.search(text);
    const ids = results.map(result => result.id);
    if (!ids.length) {
      return [];
    }
    return this.wardModel.find({ _id: { $in: ids } }).exec();
  }
}
