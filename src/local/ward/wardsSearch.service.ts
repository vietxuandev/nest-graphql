import { Injectable } from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { Ward } from './ward.schema';
import WardSearchBody from './types/wardSearchBody.interface';
import WardSearchResult from './types/wardSearchResult.interface';

@Injectable()
export class WardsSearchService {
  index = 'wards';
  constructor(private readonly elasticsearchService: ElasticsearchService) {}

  async indexWard(ward: Ward) {
    return this.elasticsearchService.index<WardSearchResult, WardSearchBody>({
      index: this.index,
      body: {
        id: ward.id,
        name: ward.name,
        prefix: ward.prefix,
      },
    });
  }

  async search(text: string) {
    const { body } = await this.elasticsearchService.search<WardSearchResult>({
      index: this.index,
      body: {
        query: {
          multi_match: {
            query: text,
            fields: ['name', 'prefix'],
          },
        },
      },
    });
    const hits = body.hits.hits;
    return hits.map(item => item._source);
  }
}
