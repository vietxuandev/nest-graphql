import { Field, InputType } from '@nestjs/graphql';

import { AllowedRoles } from 'src/common/enums/roles.enum';

@InputType()
export class SignUpInput {
  @Field(() => String, { nullable: false })
  email: string;

  @Field(() => String, { nullable: false })
  phoneNumber?: string;

  @Field(() => String, { nullable: true })
  firstName?: string;

  @Field(() => String, { nullable: true })
  lastName?: string;

  @Field(() => String, { nullable: false })
  password: string;

  @Field(() => [AllowedRoles], { nullable: true })
  roles: [AllowedRoles];
}

@InputType()
export class SignInInput {
  @Field(() => String, { nullable: false })
  email: string;

  @Field(() => String, { nullable: false })
  password: string;
}

@InputType()
export class AddRoleInput {
  @Field(() => String, { nullable: false })
  email: string;

  @Field(() => AllowedRoles, { nullable: false })
  role: AllowedRoles;
}
