import { Field, ID, ObjectType } from '@nestjs/graphql';

import { AllowedRoles } from 'src/common/enums/roles.enum';
import { FileOutput } from 'src/file/dto/file.output';

@ObjectType()
export class UserOutput {
  @Field(() => ID, { nullable: true })
  _id?: string;

  @Field(() => String, { nullable: true })
  email?: string;

  @Field(() => String, { nullable: true })
  phoneNumber?: string;

  @Field(() => String, { nullable: true })
  firstName?: string;

  @Field(() => String, { nullable: true })
  lastName?: string;

  @Field(() => FileOutput, { nullable: true })
  avatar?: FileOutput;

  @Field(() => [AllowedRoles], { nullable: true })
  roles?: AllowedRoles[];
}
