import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';

import { AuthService } from 'src/auth/auth.service';
import { GqlExecutionContext } from '@nestjs/graphql';
import { Observable } from 'rxjs';

@Injectable()
export class UserGuard implements CanActivate {
  constructor(private authService: AuthService) {}
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const ctx = GqlExecutionContext.create(context).getContext();
    const authorization =
      ctx.req?.headers?.authorization ?? ctx.req?.Authorization;
    if (authorization) {
      ctx.user = this.authService.validateToken(authorization);
    }
    return true;
  }
}
