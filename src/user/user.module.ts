import * as bcrypt from 'bcrypt';

import { User, UserSchema } from './user.schema';

import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserResolver } from './user.resolver';
import { UserSeed } from './user.seed';
import { UserService } from './user.service';
import { FileModule } from 'src/file/file.module';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: User.name,
        useFactory: () => {
          const schema = UserSchema;
          schema.pre<User>('save', async function() {
            this.password = await bcrypt.hash(this.password, 10);
          });
          return schema;
        },
      },
    ]),
    FileModule,
  ],
  providers: [UserResolver, UserService, UserSeed],
  exports: [UserService, UserSeed],
})
export class UserModule {}
