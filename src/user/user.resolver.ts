import { HttpException, HttpStatus, UseGuards } from '@nestjs/common';
import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { AllowedRoles } from 'src/common/enums/roles.enum';
import { UserService } from './user.service';
import { FileUpload, GraphQLUpload } from 'graphql-upload';
import { createWriteStream } from 'fs';
import { v4 as uuid } from 'uuid';
import { Auth } from 'src/auth/auth.decorator';
import { CurrentUser } from './user.decorator';
import { User } from './user.schema';
import { UserOutput } from './dto/user.output';
import { AddRoleInput } from './dto/user.input';
import { AuthGuard } from 'src/auth/auth.guard';

@Resolver('User')
export class UserResolver {
  constructor(private readonly userService: UserService) {}

  @Query(() => [UserOutput])
  async users(
    @Args('searchText', { type: () => String, nullable: true })
    searchText?: string,
  ) {
    if (searchText) {
      return await this.userService.findBySearchText(searchText);
    }
    return await this.userService.findAll();
  }

  @Mutation(() => UserOutput)
  @Auth(AllowedRoles.ADMIN)
  async addRole(@Args('input') input: AddRoleInput) {
    return await this.userService.addRole(input.email, input.role);
  }

  @Mutation(() => UserOutput)
  @UseGuards(AuthGuard)
  async uploadAvatar(
    @Args('file', { type: () => GraphQLUpload })
    file: Promise<FileUpload>,
    @CurrentUser() user: User,
  ) {
    const { createReadStream, filename, mimetype } = await file;
    if (mimetype.split('/')[0] === 'image') {
      const id = uuid();
      const path = `./public/images/${id}-${filename}`;
      return new Promise(async (resolve, reject) =>
        createReadStream()
          .pipe(createWriteStream(path))
          .on('finish', async () => {
            const currentUser = await this.userService.findByIdAndUpdate(
              user._id,
              {
                avatarUrl: `/images/${id}-${filename}`,
              },
            );
            resolve(currentUser);
          })
          .on('error', error => reject(error)),
      );
    } else {
      throw new HttpException('Avatar must be an image!', HttpStatus.FORBIDDEN);
    }
  }
}
