import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

import { AllowedRoles } from 'src/common/enums/roles.enum';
import { Document, Types } from 'mongoose';
import { IsEmail } from 'class-validator';
import { File } from 'src/file/file.schema';

@Schema({ timestamps: true })
export class User extends Document {
  @Prop({ type: String, required: true })
  @IsEmail()
  email: string;

  @Prop({ type: String, required: true })
  phoneNumber?: string;

  @Prop({ type: String, required: false })
  firstName?: string;

  @Prop({ type: String, required: false })
  lastName?: string;

  @Prop({ type: String, required: true })
  password: string;

  @Prop({ type: [String], default: [AllowedRoles.CUSTOMER] })
  roles: string[];

  @Prop({
    type: Types.ObjectId,
    required: false,
    ref: File.name,
    autopopulate: true,
  })
  avatar?: File;
}

export const UserSchema = SchemaFactory.createForClass(User);
