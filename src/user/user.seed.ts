import { AllowedRoles } from 'src/common/enums/roles.enum';
import { Command } from 'nestjs-command';
import { Injectable } from '@nestjs/common';
import { UserService } from './user.service';
import readline from 'readline';

@Injectable()
export class UserSeed {
  constructor(private readonly userService: UserService) {}

  @Command({
    command: 'create:admin',
    describe: 'Create Admin user',
    autoExit: true,
  })
  async create() {
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });

    function question(theQuestion: string) {
      return new Promise<string>(resolve =>
        rl.question(theQuestion, answ => resolve(answ)),
      );
    }

    const email = await question('Email: ');
    const phoneNumber = await question('Phone number: ');
    const firstName = await question('First name: ');
    const lastName = await question('Last name: ');
    const password = await question('Password: ');

    const user = await this.userService.findByEmail(email);
    if (!user) {
      const user = await this.userService.create({
        email,
        phoneNumber,
        firstName,
        lastName,
        password,
        roles: [AllowedRoles.ADMIN],
      });
      user.password = password;
      console.log('Admin user was created. Detail: ', user);
    } else {
      console.log('Admin user already exists');
    }
  }
}
