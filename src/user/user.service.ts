import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './user.schema';
import { AllowedRoles } from 'src/common/enums/roles.enum';
import { SignUpInput } from './dto/user.input';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name)
    private readonly userModel: Model<User>,
  ) {}

  async findAll(): Promise<User[]> {
    return this.userModel.find().exec();
  }

  async findBySearchText(searchText: string): Promise<User[]> {
    return this.userModel
      .find(
        { $text: { $search: searchText } },
        { score: { $meta: 'textScore' } },
      )
      .sort({ score: { $meta: 'textScore' } })
      .exec();
  }

  async findById(id: string): Promise<User | undefined> {
    return this.userModel.findById(id).exec();
  }

  async findByIdAndUpdate(id: string, update: any) {
    return this.userModel.findByIdAndUpdate(id, update, { new: true }).exec();
  }

  async findByEmail(email: string): Promise<User | undefined> {
    return this.userModel.findOne({ email }).exec();
  }

  async create(input: SignUpInput): Promise<User> {
    const createdUser = new this.userModel(input);
    return createdUser.save();
  }

  async createAdmin(input: SignUpInput): Promise<User> {
    const createdUser = new this.userModel(input);
    createdUser.roles = [AllowedRoles.ADMIN];
    return createdUser.save();
  }

  async addRole(email: string, role: string): Promise<User> {
    const user = await this.findByEmail(email);
    if (!user) {
      throw new NotFoundException('Email is not existed');
    }
    if (user.roles.indexOf(role) === -1) {
      user.roles.push(role);
      return user.save();
    } else {
      throw new HttpException('Role is existed', HttpStatus.CONFLICT);
    }
  }
}
